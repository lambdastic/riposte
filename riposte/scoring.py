import nltk


class Score:
    def __init__(self, recall, precision, f_score):
        self.recall = recall
        self.precision = precision
        self.f_score = f_score


def _score_simple(sentence, answer):
    assert "|" not in answer
    sentencewords = nltk.word_tokenize(sentence)
    answerwords = nltk.word_tokenize(answer)
    correct_words_in_answer = 0
    answer_words_copy = answerwords[:]
    for word in sentencewords:
        if word in answer_words_copy:
            answer_words_copy.remove(word)
            correct_words_in_answer += 1
    recall = correct_words_in_answer / len(answerwords)
    precision = correct_words_in_answer / len(sentencewords)
    try:
        f_score = 2 * precision * recall / (precision + recall)
    except ZeroDivisionError:
        f_score = 0
    return Score(recall, precision, f_score)


def score(sentence, answer):
    parts = answer.split("|")
    scores = [_score_simple(sentence, p) for p in parts]
    return max(scores, key=lambda x: x.f_score)
