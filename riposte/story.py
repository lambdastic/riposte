import nltk

from .chunker import np_chunk
import sys

debug = "-V" in sys.argv

class Sentence:
    def __init__(self, text):
        self.text = text
        self.words = nltk.word_tokenize(text)
        self.words_postagged = nltk.pos_tag(self.words)
        self.words_ne_tagged = nltk.ne_chunk(self.words_postagged)
        self.words_np_tagged = np_chunk(self.words_ne_tagged)
    
    def __eq__(self, other):
        if not isinstance(other, Sentence):
            return False
        else:
            return self.text == other.text
    
    def __hash__(self):
        return hash(self.text)
        
    
    def __repr__(self):
        return "Sentence({!r})".format(self.text)
        #return "Story(\"{}{}\")".format(self.text[:20], 
        #                                "..." if len(self.text) > 20 else "")


class Story:
    def __init__(self, storytext):
        lines = storytext.splitlines()
        thing1 = "HEADLINE:"
        if debug:
            print("Parsing", repr(lines[0][len(thing1):].strip()) + "... ", 
              end="", flush=True, file=sys.stderr)
        assert lines[0].startswith(thing1)
        self.headline = lines[0][len(thing1):].strip()
        thing2 = "DATE:"
        assert lines[1].startswith(thing2)
        self.title = lines[1][len(thing2):].strip()
        thing3 = "STORYID:"
        assert lines[2].startswith(thing3)
        self.storyid = lines[2][len(thing3):].strip()
        line_num = 3
        while not lines[line_num].startswith("TEXT:"):
            line_num += 1
        assert lines[line_num].strip() == "TEXT:"
        # skip line 5
        self.text = "\n".join(lines[line_num + 1:]).strip()
        self.sentences = [Sentence(s.replace("\n", " ")) 
                          for s in nltk.sent_tokenize(self.text)]
        assert self.sentences
        s_index = 0
        while s_index < len(self.sentences):
            if self.sentences[s_index].text.lower().endswith("no.") and \
                    s_index + 1 < len(self.sentences):
                s1 = self.sentences[s_index].text
                s2 = self.sentences[s_index + 1].text
                if debug:
                    print("\nJOINING SPLIT SENTENCES:", file=sys.stderr)
                    print("\t{!r} + {!r}".format(s1, s2), file=sys.stderr)
                combined = s1 + " " + s2
                if debug:
                    print("\t=", combined, file=sys.stderr)
                self.sentences[s_index] = Sentence(combined)
                del self.sentences[s_index + 1]
            s_index += 1
        #print("Parsing", repr(lines[0][len(thing1):].strip()) + "... done")
        if debug:
            print("\rParsing", repr(lines[0][len(thing1):].strip()) + 
                "... done", file=sys.stderr)
            #for s in self.sentences:
            #    print(s.words_np_tagged[:])

    def __repr__(self):
        return "Story({!r}{})".format(self.text[:20],
                                      "..." if len(self.text) > 20 else "")

class Question:
    """
    Question(questiontext)
    
    questiontext contains questionid, question, and difficulty
    """
    def __init__(self, questionlines):
        self.textquestionlines = questionlines
        try:
            assert questionlines[0].startswith("QuestionID: ")
            self.id = questionlines[0][len("QuestionID: "):].strip()
            assert questionlines[1].startswith("Question: ")
            self.question = questionlines[1][len("Question: "):].strip()
            assert questionlines[2].startswith("Difficulty: ")
            self.difficulty = questionlines[2][len("Difficulty: "):].strip()
        except AssertionError:
            print("ASSERTION ERROR")
            import pprint
            pprint.pprint(questionlines)
            raise
        
        self.question_words = nltk.word_tokenize(self.question)
        self.question_postagged = nltk.pos_tag(self.question_words)
        self.question_ne_tagged = nltk.ne_chunk(self.question_postagged)
        self.question_np_tagged = np_chunk(self.question_ne_tagged)
    
    def __repr__(self):
        return "Question({!r})".format(self.question)
        

class QuestionAnswer(Question):
    @property
    def answer(self):
        """plain text answer as a string"""
        return self._answer
    
    @answer.setter
    def answer(self, newvalue):
        self._answer = newvalue
        self.answer_words = nltk.word_tokenize(self.answer)
        self.answer_postagged = nltk.pos_tag(self.answer_words)
        self.answer_ne_tagged = nltk.ne_chunk(self.answer_postagged)
        self.answer_np_tagged = np_chunk(self.answer_ne_tagged)
    
    def __init__(self, questionanswerlines):
        self.textlines = lines = questionanswerlines
        assert lines[0].startswith("QuestionID: ")
        self.id = lines[0][len("QuestionID: "):].strip()
        assert lines[1].startswith("Question: ")
        self.question = lines[1][len("Question: "):].strip()
        assert lines[2].startswith("Answer: ")
        self.answer = lines[2][len("Answer: "):].strip()
        assert lines[3].startswith("Difficulty: ")
        self.difficulty = lines[3][len("Difficulty: "):].strip()
        
        self.question_words = nltk.word_tokenize(self.question)
        self.question_postagged = nltk.pos_tag(self.question_words)
        self.question_ne_tagged = nltk.ne_chunk(self.question_postagged)
        self.question_np_tagged = np_chunk(self.question_ne_tagged)
                
        #if debug:
        #    print(self.question_np_tagged, file=sys.stderr)
        #    print(self.answer_np_tagged, file=sys.stderr)
        #    print(file=sys.stderr)

    def __repr__(self):
        return "QuestionAnswer({!r})".format(self.question, self.answer)
