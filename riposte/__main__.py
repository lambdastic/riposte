from .storyquestions import get_story_questions_instances
from .timing import Timing

import sys, pdb

debug = "-v" in sys.argv or "-V" in sys.argv


sqis = list(get_story_questions_instances(sys.argv[-1]))
all_sqis = len(sqis)
if debug:
    print("STARTING ON STORIES", file=sys.stderr)
    timing = Timing(len(sqis))
    timing.start()
for num in range(len(sqis)):
    if debug:
        print("ON STORY {} OF {}".format(num + 1, all_sqis), file=sys.stderr)
        timing.start_thing()
    #if num + 1 == 26:
    #    pdb.set_trace()
    sqi_instance = sqis[num]
    sqi_instance.process()
    sqi_instance.answer_questions(sys.stdout)
    if debug:
        timing.complete_thing()
if debug:
    print("DONE", file=sys.stderr)
    timing.all_done()
