import nltk

_grammar = """\
NP: {<CD|DT|PDT>?(<RB|RBR|RBS>*<JJ|JJR>)*<NN|NNP|NNPS|NNS>+}
"""

_parser = nltk.RegexpParser(_grammar)

#sentence = "The really dumb extremely lazy excruciatingly painful subject pursued us to the bad baseball field."

def test():
    sentence = "The subject was boring"

    sentences = """
    A middle school in Liverpool, Nova Scotia is pumping up bodies as well
    as minds.

    It's an example of a school teaming up with the community to raise
    money. South Queens Junior High School is taking aim at the fitness
    market.

    The school has turned its one-time metal shop - lost to budget cuts
    almost two years ago - into a money-making professional fitness club.
    The club will be open seven days a week.

    The club, operated by a non-profit society made up of school and
    community volunteers, has sold more than 30 memberships and hired a
    full-time co-ordinator.

    Principal Betty Jean Aucoin says the club is a first for a Nova Scotia
    public school. She says the school took it on itself to provide a
    service needed in Liverpool.

    "We don't have any athletic facilities here on the South Shore of Nova
    Scotia, so if we don't use our schools, communities such as Queens are
    going to be struggling to get anything going," Aucoin said.

    More than a $100,000 was raised through fund-raising and donations from
    government, Sport Nova Scotia, and two local companies.

    Some people are wondering if the ties between the businesses and the
    school are too close. Schools are not set up to make profits or promote
    businesses.  

    Southwest Regional School Board superintendent Ann Jones says there's no
    fear the lines between education and business are blurring.

    "First call on any school facility belongs to... the youngsters in the
    school," says Ann Jones.

    The 12,000-square-foot club has seven aerobic machines, including
    treadmills, steppers, and stationary bicycles, as well as weight
    machines and freeweights.

    Memberships cost $180 a year for adults and $135 for students and
    seniors.

    Proceeds pay the salary of the centre co-ordinator and upkeep of the
    facility.
    """

    tokens = nltk.word_tokenize(sentences)
    tags = nltk.pos_tag(tokens)
    print(tokens)
    print(tags)
    chunks = cp.parse(tags)
    print(chunks)


def np_chunk(tagged_words):
    return _parser.parse(tagged_words)
