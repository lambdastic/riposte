import nltk
import nltk.parse.stanford
import os

cp = nltk.load_parser("grammars/large_grammars/atis.cfg")

thisdir = os.path.dirname(__file__)

parser=nltk.parse.stanford.StanfordParser(
    path_to_jar=os.path.join(thisdir, "stanford-parser.jar"),
    path_to_models_jar=os.path.join(thisdir, "stanford-parser-3.5.2-models.jar"),
    model_path="edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz"
)

def parse(tagged_stuff):
    print(parser.tagged_parse(tagged_stuff))