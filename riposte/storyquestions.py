import os, sys
from . import story
from .answering import get_question_answer_simple, get_question_answer

debug = "-V" in sys.argv


class StoryQuestionsInstance:
    def __init__(self, storypath, questionanswerspath):
        self.storypath = storypath
        self.questionanswerspath = questionanswerspath
        self.storytext = None
        self.story = None
        self.questionstext = None
    
    def process(self):
        self._add_story_file_text()
        self._add_story_instance()
        self._add_questions_text()
        self._add_questions_instances()
    
    def _add_story_file_text(self):
        try:
            f = open(self.storypath)
        except FileNotFoundError:
            print("storypath is", self.storypath)
            raise
        
        with f:
            self.storytext = f.read()
    
    def _add_story_instance(self):
        self.story = story.Story(self.storytext)
    
    def _add_questions_text(self):
        if debug:
            print("ADD QUESTIONS TEXT:", self.questionanswerspath, 
                file=sys.stderr)
        with open(self.questionanswerspath) as f:
            self.questionstextlines = [x.strip("\n") for x in f.readlines()]
    
    def _add_questions_instances(self):
        self.questions = [story.Question(self.questionstextlines[x:x + 3])
                          for x in range(0, len(self.questionstextlines), 4)]
    
    def answer_questions(self, file):
        for question in self.questions:
            #answer = get_question_answer_simple(self.story, question)
            answer = get_question_answer(self.story, question)
            print("QuestionID:", question.id, file=file)
            print("Answer:", answer, end="\n\n", file=file)


def get_story_questions_instances(filename):
    """
    get_story_question_instances(filename) -> iterator
    """
    with open(filename, 'rU') as f:
        dirname = f.readline().strip("\n")
        stems = [thing.strip('\n') for thing in f.readlines()]
    
    for s in filter(None, stems):
        questionspath = os.path.join(dirname, s + ".questions")
        storypath = os.path.join(dirname, s + ".story")
        yield StoryQuestionsInstance(storypath, questionspath)
