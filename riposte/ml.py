from abc import abstractmethod, ABCMeta
from math import log
#from time import process_time
import cProfile, sys, pdb

debug = print_debug = "-V" in sys.argv


class Feature(type):
    """
    Feature is metaclass
    
    The instance of Feature must have a labels class attribute and a _getlabel
    class method.
    
    Use like this:
    
    class MyFeature(metaclass=Feature):
        labels = ["0", "1", "2"]
        
        @classmethod
        def _getlabels(cls, data):
            return "1"
    """
    
    def getlabel(self, data):
        """
        feature.getlabel(data) -> str
        
        data: (story, question, sentence)
        
        story: Story object
        question: Question object
        sentence: Sentence object
        
        calls feature._getlabel(data) which must be a classmethod
        """
        result = self._getlabel(data)
        assert isinstance(result, str)
        if result not in self.labels:
            print("GOT WEIRD LABEL:", result)
            raise AssertionError()
        return result

    


class Node:
    def __init__(self, parent=None):
        self._parent = parent
        self._children = dict()
        self._leafnode = True
        self._feature = None
        self._informationgain = None
        self._label = None  # boolean
    
    def __repr__(self):
        thing = "Node(feature={}, leafnode={}, label={}, informationgain={}, children={})"
        return thing.format(self._feature,
                            self._leafnode, 
                            self._label, 
                            self._informationgain,
                            self._children)
    
    def to_dict(self):
        children = self._children.copy()
        for c in children:
            children[c] = children[c].to_dict()
        
        return {'feature': self._feature,
                'leafnode': self._leafnode,
                'label': self._label,
                'informationgain': self._informationgain,
                'children': children}
    
    def addchild(self, child):
        newnode = Node(self)
        self._children[child] = newnode
        self._leafnode = False
        return newnode
    
    def removechild(self, child):
        del self._children[child]
        self._leafnode = len(self._children) == 0
    
    def getchild(self, label):
        if self._feature == None:
            return None
        childname = self._feature.getlabel(label)
        return self._children[childname]
    
    def addfeature(self, feature):
        self._feature = feature
    
    @property
    def isleafnode(self):
        return self._leafnode


class Id3: # decision tree model
    def __init__(self):
        self._start = Node(None)
        self._generatedtree = False
        self._infuse_node_calls = 0
        
    def __repr__(self):
        return "Id3(start={})".format(self._start)
    
    # external method
    def generatetree(self, trainingdata, features, depth=1):
        """
        self.generatetree(trainingdata, features)
        
        trainingdata: dict[(story, sentence, question)] -> bool
        features: list
        """
        features = list(features)  # copy
        self._infuse_node(self._start, trainingdata, features, dict(), depth)
    
    # external method
    def create_label(self, value):
        """
        self.create_label(value)
        
        value: tuple of (story, question, sentence)
        """
        #import pdb
        #pdb.set_trace()
        #print("IN CREATE LABEL; _start._label =", self._start._label)
        follow = self._start
        while not follow.isleafnode:
            follow = follow.getchild(value)
        #print("FOUND NODE:", id(follow), follow)
        return follow._label
    
    def create_informationgain(self, value):
        """
        self.create_informationgain(value)
        
        value: tuple of (story, question, sentence)
        """
        follow = self._start
        while not follow.isleafnode:
            follow = follow.getchild(value)
        return follow._informationgain
    
    def _infuse_node(self, node, trainingdata, features, usedfeatures, depth):
        """
        self._infuse_node(node, trainingdata, features, usedfeatures)
        
        node: Node
        trainingdata: dict[(story, sentence, question)] -> bool
        features: list of Feature
        usedfeatures: dict[Feature] -> str
        """
        feature = None
        
        if print_debug:
            print("Calling Id3._infuse_node()", file=sys.stderr)
            print("\tthis is call number", self._infuse_node_calls, 
                    file=sys.stderr)
            self._infuse_node_calls += 1
            #print("\tsystem clock is", process_time())
            print("\tfeatures list is:", features, file=sys.stderr)
        
        #if print_debug:
        #    print("\nId3._infuse_node(): calculating total entropy")
        total_entropy = self._entropy(trainingdata, usedfeatures)
        information_gain = 0
        
        for curfeature in features:
            #if print_debug:
            #    print("Id3._infuse_node(): starting feature", repr(curfeature))
            curinformation_gain = 0
            for curlabel in curfeature.labels:
                heathlist = dict(usedfeatures)
                heathlist[curfeature] = curlabel
                curinformation_gain += self._entropy(trainingdata, heathlist)
            curinformation_gain = total_entropy - curinformation_gain
            if feature == None or information_gain < curinformation_gain:
                information_gain = curinformation_gain
                feature = curfeature
            #if print_debug:
            #    print("Id3._infuse_node(): finished with feature", 
            #          repr(curfeature))
        #if print_debug:
        #    print("Id3._infuse_node(): done with all features")
        
        if feature == None or total_entropy == information_gain or depth == 0:
            matches = 0
            positivelabels = 0
            for curname in trainingdata.keys():
                if self._labelmatch(curname, usedfeatures):
                    matches += 1
                    if trainingdata[curname]:
                        positivelabels += 1
            node._leafnode = True
            node._label = positivelabels >= matches - positivelabels
            if matches == 0:
                node._informationgain = 1.0
            else:
                node._informationgain = positivelabels / matches
        else:
            for i in range(len(features)):
                if features[i] == feature:
                    features = features[:]
                    del features[i]
                    break
            nextdepth = depth - 1 if depth != None else None
            for curlabel in feature.labels:
                partialtrainingdata = dict()
                for k in trainingdata:
                    if feature.getlabel(k) == curlabel:
                        partialtrainingdata[k] = trainingdata[k]
                heathlist = dict(usedfeatures)
                heathlist[feature] = curlabel
                
                if debug:
                    pass
                    #if self._infuse_node_calls >= 4:
                    #    print("TOO MANY INFUSE NODE CALLS")
                    #    return
                    
                self._infuse_node(node.addchild(curlabel), 
                                  partialtrainingdata, 
                                  features,
                                  heathlist,
                                  nextdepth)
            node.addfeature(feature)
    
    def _entropy(self, trainingdata, equalizers):
        """
        self.entropy(trainingdata, equalizers) -> number
        
        trainingdata: dict[string] -> bool
        equalizers: dict[Feature] -> str
        """
        matches = 0
        positivelabels = 0
        keynum = 0
        totalkeys = len(trainingdata.keys())
        for datum in trainingdata.keys():
            if print_debug:
                pass
                #print("\tId3._entropy(): processing datum {} of {}".format(
                #        keynum,
                #        totalkeys))
            assert isinstance(datum, tuple)
            if self._labelmatch(datum, equalizers):
                matches += 1
                if trainingdata[datum]:
                    positivelabels += 1
            keynum += 1
        if matches == 0:
            return 0
        p = float(positivelabels) / matches
        n = 1 - p
        entropy = matches / len(trainingdata)
        if n != 0 and p != 0:
            return entropy * -p * (log(p, 2) - n * log(n, 2))
        else:
            return 0
    
    def _labelmatch(self, name, equalizers):
        assert isinstance(name, tuple)
        result = True
        if equalizers != None:
            for feature in equalizers.keys():
                result = result and \
                         feature.getlabel(name) == equalizers[feature]
        return result
