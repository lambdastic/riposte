from .ml import Feature, Node, Id3
from .story import QuestionAnswer, Story, Sentence
from .scoring import score as generate_score
from . import features
from .timing import Timing, format_elapsed_time #, seconds_to_string, 

import os, random, pickle, sys, pprint, time

training_data_folder = "training-data"

tree_filename = "tree.pickle"
mappings_pickle_file = "mappings.pickle"

dirname = os.path.dirname(__file__)

decisiontree = None

superdebug = "-V" in sys.argv
debug = "-v" in sys.argv or superdebug

if not os.path.exists(os.path.join(dirname, tree_filename)) \
        or "-r" in sys.argv or "-m" in sys.argv:
    if not os.path.exists(os.path.join(dirname, mappings_pickle_file)) \
            or "-m" in sys.argv:
        if debug:
            print("REGENERATING MAPPINGS", file=sys.stderr)
            #start_time = time.time()
            #print("\tstarting at", time.ctime(start_time), file=sys.stderr)
        mappings = dict()
    
        files = [file for file in 
                 os.listdir(os.path.join(dirname, training_data_folder))
                 if file.endswith(".answers")]
        #times = []
        if debug:
            timing = Timing(len(files))
            timing.start()

        for index, fname in enumerate(files):
            if debug:
                thing = "PROCESSING FILE {} of {}: {!r}" \
                    .format(index + 1, len(files), fname)
                print(thing, file=sys.stderr)
                #this_fname_start = time.time()
                #print("\telapsed time at start:", 
                #      format_elapsed_time(start_time, this_fname_start),
                #      file=sys.stderr)
                timing.start_thing()
            with open(os.path.join(dirname, training_data_folder, fname)) as f:
                lines = list(filter(lambda x: x.strip(), f.readlines()))
        
            stem = fname[:-len(".answers")]
            storyf = os.path.join(dirname, training_data_folder, stem + ".story")
            with open(storyf) as f:
                story = Story(f.read())
        
            qa_dict = dict()
            for x in range(0, len(lines), 4): # increment by 5
                qa = QuestionAnswer(lines[x:x + 4])
                
                scores = [generate_score(s.text, 
                                         qa.answer + " " + qa.question).f_score
                          for s in story.sentences]
                # overwrite real answer with sentence we think has the answer
                #qa.answer = story.sentences[scores.index(max(scores))].text
                scores_copy = scores[:]
                badsentences = []
                scores_sorted = list(sorted(scores, reverse=True))
                
                assert scores_sorted[0] > scores_sorted[-1]
                index = scores_copy.index(scores_sorted[0])
                qa.answer = story.sentences[index].text
                scores_copy[index] = None
                
                for x in range(1, 4):
                    index = scores_copy.index(scores_sorted[x])
                    sentencetext = story.sentences[index].text \
                        .strip().replace("\n", " ")
                    badsentences.append(sentencetext)
                
                qa_dict[qa.id] = qa
            
                #sentences = story.sentences[:]
                #random.shuffle(sentences)
                
                #for s in sentences:
                #    if s.text.strip() != qa.answer:
                #        badsentences.append(s.text.strip().replace("\n", " "))
                #    if len(badsentences) == 3:
                #        break
                assert len(badsentences) == 3
            
                for bs in badsentences:
                    mappings[(story, qa, Sentence(bs))] = False
                mappings[(story, qa, Sentence(qa.answer))] = True
            if debug:
                #complete_time = time.time()
                #print("\tcompleted in", 
                #      format_elapsed_time(this_fname_start, complete_time),
                #      file=sys.stderr)
                #times.append(complete_time - this_fname_start)
                #average_time = sum(times) / len(times)
                #itemsleft = len(files) - len(times)
                #time_remaining = average_time * itemsleft
                #print("\tprojected time remaining:", 
                #      seconds_to_string(time_remaining),
                #      file=sys.stderr)
                timing.complete_thing()
                
        if debug:
            #print("\telapsed time:", 
            #      format_elapsed_time(start_time, time.time()),
            #      file=sys.stderr)
            print("DUMPING MAPPINGS...", file=sys.stderr)
            timing.start_post()
                
        with open(os.path.join(dirname, mappings_pickle_file), "wb") as f:
            pickle.dump(mappings, f)
        if debug:
            print("DUMPED MAPPINGS NAMED", repr(mappings_pickle_file), 
                    file=sys.stderr)
            #done_time = time.time()
            #print("\tfinished at", 
            #      time.ctime(done_time), 
            #      "({})".format(format_elapsed_time(start_time, done_time)),
            #      file=sys.stderr)
            timing.all_done()
    else:
        with open(os.path.join(dirname, mappings_pickle_file), "rb") as f:
            mappings = pickle.load(f)
        if debug:
            print("LOADED MAPPINGS NAMED", repr(mappings_pickle_file),
                file=sys.stderr)
    #print(mappings)
    
    features = [features.DiffFromMaxWordMatchFeature,
                #features.DMWM_Prev_Feature,
                #features.DMWM_Next_Feature,
                features.InterrogativeDeterminer,
                features.CountPersons,
                features.CountLocations,
                features.CountGPEs]
    #features = [features.DiffFromMaxWordMatchFeature()]
                
    decisiontree = Id3()
    
    if debug:
        print("REGENERATING DECISION TREE", file=sys.stderr)
        start_time = time.time()
        print("\tstarting at", time.ctime(start_time), file=sys.stderr)
        
    decisiontree.generatetree(mappings, features)
    
    #print(myid3)
    #print("Done.")
    
    with open(os.path.join(dirname, tree_filename), "wb") as f:
        pickle.dump(decisiontree, f)
    
    if debug:
        print("DUMPED TREE TO", os.path.join(dirname, tree_filename), 
            file=sys.stderr)
        print("\tfinished at", 
              time.ctime(start_time), 
              "({})".format(format_elapsed_time(start_time, time.time())),
              file=sys.stderr)

else:
    if debug:
        print("FOUND TREE AT", os.path.join(dirname, tree_filename),
            file=sys.stderr)
        
    with open(os.path.join(dirname, tree_filename), "rb") as f:
        decisiontree = pickle.load(f)
        
    if debug:
        print("LOADED TREE", file=sys.stderr)

    if "--print-tree" in sys.argv:
        print("Decision tree:", file=sys.stderr)
        print(decisiontree, file=sys.stderr)
        sys.exit(0)
        

if superdebug:
    print(object.__repr__(decisiontree), file=sys.stderr)
    pprint.pprint(decisiontree._start.to_dict(), stream=sys.stderr)
#import pdb
#pdb.set_trace()
