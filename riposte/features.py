from .ml import Feature
from .english_stopwords import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk.stem.lancaster import LancasterStemmer
from nltk import Tree

from pprint import pprint

import sys

debug = "-V" in sys.argv

stopwords = set(stopwords)
wnl = WordNetLemmatizer()
porterstemmer = PorterStemmer()
lancasterstemmer = LancasterStemmer()

stemstash_wordnet = dict()
stemstash_porter = dict()
stemstash_lancaster = dict()


def mylemmatize_wordnet(word):
    #print("Lemmatizing word", repr(word), end="", flush=True)
    assert word
    if stemstash_wordnet.get(word):
        return word
    stem = wnl.lemmatize(word)
    #stem = porterstemmer.stem(word)
    #stem = lancasterstemmer.stem(word)
    stemstash_wordnet[word] = stem
    #print("\rLemmatizing word", repr(word), "->", repr(stem))
    return stem

def mylemmatize_lancaster(word):
    #print("Lemmatizing word", repr(word), end="", flush=True)
    assert word
    if stemstash_lancaster.get(word):
        return word
    #stem = wnl.lemmatize(word)
    #stem = porterstemmer.stem(word)
    stem = lancasterstemmer.stem(word)
    stemstash_lancaster[word] = stem
    #print("\rLemmatizing word", repr(word), "->", repr(stem))
    return stem


def mylemmatize_porter(word):
    #print("Lemmatizing word", repr(word), end="", flush=True)
    assert word
    if stemstash_porter.get(word):
        return word
    #stem = wnl.lemmatize(word)
    stem = porterstemmer.stem(word)
    #stem = lancasterstemmer.stem(word)
    stemstash_porter[word] = stem
    #print("\rLemmatizing word", repr(word), "->", repr(stem))
    return stem


def remove_stop_words_and_stem(lst):
    #fixed_wordnet = [mylemmatize_wordnet(word)
    #                 for word in lst if word not in mystopwords]
    #fixed_porter = [mylemmatize_porter(word)
    #                for word in lst if word not in mystopwords]
    fixed_lancaster = [mylemmatize_lancaster(word.lower()) #compare lowercase
                       for word in lst if word not in stopwords]
    
    #print(lst)
    #print(repr(fixed_wordnet))
    #print(repr(fixed_porter))
    #print(repr(fixed_lancaster))
    #print()
    return fixed_lancaster


def common_word_count(list1, list2):
    word_count = 0
    list2 = list2[:]
    for word in list1:
        if word in list2:
            list2.remove(word)
            word_count += 1
    return word_count


def find_max_word_concurrence(story, question_prime):
    return max(common_word_count(remove_stop_words_and_stem(s.words),
                                 question_prime)
               for s in story.sentences)

class LabelsFrom0To1:
    # from "0.0" to "1.0"
    labels = [str(x / 10) for x in range(0, 12, 2)]


class DiffFromMaxWordMatchFeature(LabelsFrom0To1, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        """returns something from self.labels"""
        (story, question, sentence) = data
        
        import pdb 
        #pdb.set_trace()
        
        question_prime = remove_stop_words_and_stem(question.question_words)
        #print("question_prime:", question_prime)
        sentence_prime = remove_stop_words_and_stem(sentence.words)
        #print("sentence_prime:", sentence_prime)

        max_common_words = find_max_word_concurrence(story, question_prime)
        #print("max_common_words:", max_common_words)
        if max_common_words == 0:
            return "0.0"
        cwc = common_word_count(sentence_prime, question_prime)
        #print("common word count:", cwc)
        if max_common_words < cwc:
            max_common_words = cwc
        intermediate = cwc / (2 * max_common_words)
        #print("intermediate:", intermediate)
        ret = str(round(intermediate, 1) * 2)  # round to 1 decimal place
        #print("returning", ret)
        #print()
        #if debug:
        #    print("In DiffFromMaxWordMatchFeature._getlabel(), returning", 
        #          ret, file=sys.stderr)
        return ret


class DMWM_Prev_Feature(LabelsFrom0To1, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        
        try:
            sentence_index = story.sentences.index(sentence)
        except ValueError:
            print("SENTENCE NOT IN story.sentences", file=sys.stderr)
            print(sentence, file=sys.stderr)
            pprint(story.sentences, stream=sys.stderr)
            raise

        if sentence_index == 0:
            return "0.0"
        prev_index = sentence_index - 1
        question_prime = remove_stop_words_and_stem(question.question_words)
        sentence_prime = \
            remove_stop_words_and_stem(story.sentences[prev_index].words)

        max_common_words = find_max_word_concurrence(story, question_prime)
        if max_common_words == 0:
            return "0.0"
        cwc = common_word_count(sentence_prime, question_prime)
        #print("common word count:", cwc)
        if max_common_words < cwc:
            max_common_words = cwc
        intermediate = (common_word_count(sentence_prime, question_prime) /
                         (2 * max_common_words)) # !!!max_common_words is 0!!!
        ret = str(round(intermediate, 1) * 2)  # round to 1 decimal place
        #if debug:
        #    print("In DMWM_Prev_Feature._getlabel(), returning", 
        #          ret, file=sys.stderr)
        return ret


class DMWM_Next_Feature(LabelsFrom0To1, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        
        try:
            sentence_index = story.sentences.index(sentence)
        except ValueError:
            print("SENTENCE NOT IN story.sentences", file=sys.stderr)
            print(sentence, file=sys.stderr)
            pprint(story.sentences, stream=sys.stderr)
            raise
        
        if sentence_index == len(story.sentences) - 1:
            return "0.0"
        next_index = sentence_index + 1
        question_prime = remove_stop_words_and_stem(question.question_words)
        sentence_prime = \
            remove_stop_words_and_stem(story.sentences[next_index].words)

        max_common_words = find_max_word_concurrence(story, question_prime)
        if max_common_words == 0:
            return "0.0"
        cwc = common_word_count(sentence_prime, question_prime)
        #print("common word count:", cwc)
        if max_common_words < cwc:
            max_common_words = cwc
        intermediate = (common_word_count(sentence_prime, question_prime) /
                         (2 * max_common_words)) # !!!max_common_words is 0!!!
        ret = str(round(intermediate, 1) * 2)  # round to 1 decimal place
        #if debug:
        #    print("In DMWM_Next_Feature._getlabel(), returning", 
        #          ret, file=sys.stderr)
        return ret


class InterrogativeDeterminer(metaclass=Feature):
    labels = ["who", "what", "when", "where", "why", "how", "<NONE>"]
    
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        for word in question.question_words:
            word = word.lower()
            if word in (cls.labels + ["whose", "whom"]):
                if word in "who whose whom".split():
                    return "who"
                else:
                    return word
        else:
            return "<NONE>"


class Numbers:
    max_nums = 5
    labels = list(map(str, range(max_nums + 1))) + ["TOO MANY"]
    


class CountPersons(Numbers, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        personcount = 0
        for doobie in sentence.words_np_tagged:
            if isinstance(doobie, Tree) and doobie.label() == "PERSON":
                personcount += 1
        return "TOO MANY" if personcount >= Numbers.max_nums else str(personcount)


class CountLocations(Numbers, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        loc_count = 0
        for doobie in sentence.words_np_tagged:
            if isinstance(doobie, Tree) and doobie.label() == "LOCATION":
                loc_count += 1
        return "TOO MANY" if loc_count >= Numbers.max_nums else str(loc_count)


class CountGPEs(Numbers, metaclass=Feature):
    @classmethod
    def _getlabel(cls, data):
        (story, question, sentence) = data
        loc_count = 0
        for doobie in sentence.words_np_tagged:
            if isinstance(doobie, Tree) and doobie.label() == "GPE":
                loc_count += 1
        return "TOO MANY" if loc_count >= Numbers.max_nums else str(loc_count)
