import time, sys

def seconds_to_string(seconds):
    secondsperminute = 60
    secondsperhour = 60 * secondsperminute
    secondsperday = 24 * secondsperhour
    
    totaldays = int(seconds // secondsperday)
    secondsleft = seconds - totaldays * secondsperday
    
    totalhours = int(secondsleft // secondsperhour)
    secondsleft -= totalhours * secondsperhour
    
    totalminutes = int(secondsleft // secondsperminute)
    secondsleft -= totalminutes * secondsperminute
    
    pieces = []
    if totaldays:
        pieces.append("{} day{}".format(totaldays, 
                                        '' if totaldays == 1 else 's'))
    if totalhours:
        pieces.append("{} hour{}".format(totalhours,
                                         '' if totalhours == 1 else 's'))
    if totalminutes:
        pieces.append("{} minute{}".format(totalminutes,
                                           '' if totalminutes == 1 else 's'))
    pieces.append("{:.2f} seconds".format(secondsleft))
    
    if len(pieces) == 1:
        return pieces[0]
    elif len(pieces) == 2:
        return pieces[0] + " and " + pieces[1]
    else:
        return ", ".join(pieces[:-1]) + ", and " + pieces[-1]


def format_elapsed_time(start_time, curtime):
    """
    format_elapsed_time(start_time, curtime) -> str
    
    times are seconds since Epoch (make with time.time())
    """
    return seconds_to_string(curtime - start_time)


class Timing:
    """
    Timing(totalcountthings) -> timing object
    """
    
    def __init__(self, totalcountthings):
        self._start = None
        self._start_current = None
        self._totalcountthings = totalcountthings
        self._thingsdonesofar = 0
        self._times = []
    
    def start(self):
        self._start = time.time()
        print("\tstarting at", time.ctime(self._start), file=sys.stderr)
    
    def start_thing(self):
        assert not self._start_current
        self._start_current = time.time()
        print("\telapsed time at start:", 
              format_elapsed_time(self._start, self._start_current),
              file=sys.stderr)
        print("\tpercent complete: {:.2f}%" \
            .format(100.0 * self._thingsdonesofar / self._totalcountthings),
            file=sys.stderr)
    
    def complete_thing(self):
        self._thingsdonesofar += 1
        complete_time = time.time()
        print("\tcompleted in", 
              format_elapsed_time(self._start_current, complete_time),
              file=sys.stderr)
        self._times.append(complete_time - self._start_current)
        average_time = sum(self._times) / len(self._times)
        itemsleft = self._totalcountthings - len(self._times)
        time_remaining = average_time * itemsleft
        print("\tprojected time remaining for {} items:".format(itemsleft), 
              seconds_to_string(time_remaining),
              file=sys.stderr)
        self._start_current = None
    
    def start_post(self):
        assert not self._start_current
        final_time = time.time()
        print("\telapsed time:", 
              format_elapsed_time(self._start, final_time),
              file=sys.stderr)
 
    def all_done(self):
        done_time = time.time()
        print("\tpercent complete: {:.2f}%".format(100.0),
            file=sys.stderr)
        print("\tfinished at", 
              time.ctime(done_time), 
              "({})".format(format_elapsed_time(self._start, done_time)),
              file=sys.stderr)
