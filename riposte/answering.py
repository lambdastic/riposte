from .decisiontree import decisiontree
from .features import *
from . import features
from nltk import Tree, word_tokenize
import sys

debug = "-V" in sys.argv
yankstopwords = False

def get_question_answer(story, question):
    multiple_answer_sentences = dict()
    answersentence = None
    best_informationgain = -1
    if debug:
        print("Answering question:", question.question, file=sys.stderr)
    possible_answer_sentences = 0
    for sentence in story.sentences:
        value = (story, question, sentence)
        if decisiontree.create_label(value):
            possible_answer_sentences += 1
            informationgain = decisiontree.create_informationgain(value)
            multiple_answer_sentences[value[2]] = informationgain
            #print("GET_QUESTION_ANSWER: setting entropy to", entropy)
            if best_informationgain < informationgain:
                best_informationgain = informationgain
                answersentence = sentence
    question_words = [w.lower() for w in question.question_words]
    interrogatives = "who whom whose when where how".split()  # "what"
    interrogativefound = None
    for i in interrogatives:
        if i in question_words:
            interrogativefound = i
            break
    if debug:
        print("possible_answer_sentences =", possible_answer_sentences, 
            file=sys.stderr)
        print("all answer sentences with i-gain:", multiple_answer_sentences, 
            file=sys.stderr)
        if answersentence:
            print("Answer sentence found:", answersentence.text, 
                file=sys.stderr)
            print("Interrogative found:", interrogativefound, file=sys.stderr)
            #print(answersentence.words_np_tagged, file=sys.stderr)
        else:
            print("No answer sentence found.", file=sys.stderr)
    ret = None
    if not answersentence:
        return ""
    elif interrogativefound in ["who", "whom", "whose"]:
        persons = []
        for answersentence in multiple_answer_sentences.keys():
            persons.extend(w for w in answersentence.words_np_tagged 
                       if isinstance(w, Tree) 
                       and w.label() in ["PERSON", "GPE", "ORGANIZATION"])
        if persons:
            ret = " ".join(" ".join(x[0] for x in p.leaves()) for p in persons)
    elif interrogativefound == "when":
        times = []
        for answersentence in multiple_answer_sentences:
            times.extend(w for w in answersentence.words_np_tagged
                     if isinstance(w, Tree)
                     and w.label() == "TIME")
        if times:
            ret = " ".join(" ".join(x[0] for x in p.leaves()) for p in times)
    elif interrogativefound == "where":
        locs = []
        for answersentence in multiple_answer_sentences:
            locs.extend(w for w in answersentence.words_np_tagged
                    if isinstance(w, Tree)
                    and w.label() == ["LOCATION", 
                        "FACILITY", "ORGANIZATION", "GPE"])
        if locs:
            ret = " ".join(" ".join(x[0] for x in p.leaves()) for p in locs)
    elif interrogativefound == "how":
        things = []
        for answersentence in multiple_answer_sentences:
            things.extend(w for w in answersentence.words_np_tagged
                      if isinstance(w, Tree) and w.label() == "MONEY" 
                      or isinstance(w, tuple) and w[1] == "CD")
        if things:
            words = []
            for p in things:
                if isinstance(p, tuple):
                    words.append(p[0]) # first item
                else:
                    words.append(" ".join(x[0] for x in p.leaves()))
            ret = " ".join(words)
            
    if ret == None:
        if yankstopwords:
            ret = " ".join(w for w in answersentence.words 
                           if w.lower() not in features.stopwords)
        else:
            ret = " ".join(w for w in answersentence.words)
    ret = ret.replace(" '", "'")
    if debug:
        print("Returning this:", file=sys.stderr)
        print(repr(ret), file=sys.stderr)
    return ret


def get_question_answer_simple(story, question):
    question_prime = remove_stop_words_and_stem(question.question_words)
    
    all_sentences = dict()
    for sentence in story.sentences:
        sentence_prime = remove_stop_words_and_stem(sentence.words)
        common_words = common_word_count(question_prime, sentence_prime)
        all_sentences[sentence] = common_words
    
    most_common_words = -1
    for sentence, common_words in all_sentences.items():
        if common_words > most_common_words:
            top_sentence = sentence
            most_common_words = common_words
    return top_sentence.text